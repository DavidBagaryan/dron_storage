drones emulator

clone repo to your current dir
get inside this dir
run `docker-compose up` and make sure that will be no conflicts with other dockers on your local

after build, you can check out drones on your local `http://localhost:1337`

`http://localhost:1337` or `http://localhost:1337/drones` - get all drones

`http://localhost:1337/drones/{ID}/detail` - drone details

you can manage app by existing web GUI or make raw request depends on `OPTIONS` method
there are no tokens so far, could be added in next versions

docker-compose will run some features, e.g.:
 - base migrations
 - create superuser which will be acceseble by `http://localhost:1337/admin` with credentails user: 'admin' pass: '321456'
 - run tests
 - common initials like measure_terrain (more info by command in docker terminal path/to/app/manage.py measure_terrain -h)
 - common initials like make_some_drones (more info by command in docker terminal path/to/app/manage.py make_some_drones -h)
 - adding cronjob for changing existing drone position and runs it