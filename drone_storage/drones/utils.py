import decimal
from typing import Union


class AreaHelper:
    height: Union[int, float, decimal.Decimal] = None
    width: Union[int, float, decimal.Decimal] = None

    def get_smaller_side(self) -> Union[int, float, decimal.Decimal]:
        """getting the smaller side of the AreaModel"""

        height = self.height
        width = self.width
        return height if height < width else width

    def get_bigger_side(self) -> Union[int, float, decimal.Decimal]:
        """getting the bigger side of the AreaModel"""

        height = self.height
        width = self.width
        return height if height > width else width

    def get_sq(self) -> Union[int, float, decimal.Decimal]:
        return self.height * self.width
