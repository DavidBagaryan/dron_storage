from django.core.management import call_command


def run_emulator():
    call_command('drones_flight_emu')
