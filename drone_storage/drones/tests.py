from typing import Any

from django.test import TestCase
from django.core.management import call_command

from .models import Drone, Quadrant, AreaModel
from .serializers import QuadrantSerializer, DroneSerializer

drones_count = 331


class AreaModelTests(TestCase):
    def test_measure_terrain(self):
        """basic test command to measure terrain with pieces count and measures"""
        measures = (667, 1821)
        pieces = 173
        call_command('measure_terrain', measures=measures, pieces=pieces)
        quads = Quadrant.objects.all()
        self.assertNotEqual(quads.count(), 0)

    def test_area_pieces_equals_area_sq(self):
        """checking the equality of area square and all of the area's pieces summary squares"""
        measures = (3827, 319)
        pieces = 173
        call_command('measure_terrain', measures=measures, pieces=pieces)

        last_area_model = AreaModel.objects.filter(is_active=1).last()
        quads = Quadrant.objects.all()

        quads_summary_sq = 0
        for quad in quads:
            quads_summary_sq += quad.get_sq()

        quads_summary_sq = round(quads_summary_sq)
        last_area_model_sq = round(last_area_model.get_sq())

        self.assertEqual(quads_summary_sq, last_area_model_sq)


class DroneModelTests(TestCase):
    err_mes_not_saved = "obj not saved"
    err_mes_not_serialized = "obj not serialized"

    def test_drones_coordinates_different(self):
        """testing drones coordinates inequality"""

        call_command('measure_terrain')
        call_command('make_some_drones', count=drones_count, delete=1)

        quads = Quadrant.objects.all()
        drones = Drone.objects.all()

        self.assertIsNot(drones.count(), 0)
        self.assertIsNot(quads.count(), 0)

        current_coordinates = {}
        for drone in drones:
            new_coordinates = {drone.x, drone.y}
            self.assertNotEqual(current_coordinates, new_coordinates)
            current_coordinates = new_coordinates

    def test_all_drones_in_own_area(self):
        """test all drones on relation with own area quadrant"""

        call_command('measure_terrain')
        call_command('make_some_drones', count=drones_count, delete=1)

        drones = Drone.objects.all()

        self.assertIsNot(drones.count(), 0)

        for drone in drones:
            prev_quad = Quadrant.objects.filter(pk=drone.quadrant.id - 1)
            bottom_quad_border = prev_quad.first().border_point if prev_quad.exists() else 0
            err_mes = f'drone out of area! ID {drone.id}'
            self.assertEqual(bottom_quad_border <= drone.x <= drone.quadrant.border_point, True, err_mes)

    def test_create_area_model(self):
        AreaModel.objects.create(**{
            'height': 666,
            'width': 13,
            'is_active': 1
        })

        self.area_model = AreaModel.get_latest()
        self.assertIsNotNone(self.area_model, self.err_mes_not_saved)

    def test_create_quadrant(self):
        """test creating quad"""

        self._create_quadrant_by_attrs(height=666, width=13, border_point=13)

    def test_create_quadrant_with_string_values(self):
        """test creating quad with strings"""

        self._create_quadrant_by_attrs(height='666', width='13', border_point='13')

    def test_create_drone(self):
        """test creating quad with strings"""

        self._create_drone_by_attrs(x=12, y=665)

    def test_create_drone_with_string_values(self):
        """test creating quad with strings"""

        self._create_drone_by_attrs(x='12', y='665')

    def _create_drone_by_attrs(self, x: Any, y: Any):
        self._create_quadrant_by_attrs(height='666', width='13', border_point='13')

        drone_attrs = {'x': x, 'y': y, 'quadrant': self.quad}

        self.drone = Drone.objects.create(**drone_attrs)
        self.drone.full_clean()
        self.assertIsNotNone(self.drone, self.err_mes_not_saved)

        self.drone.delete()
        drone_attrs['quadrant'] = self.quad.id

        drone_ser = DroneSerializer(data=drone_attrs)
        self.assertEqual(drone_ser.is_valid(), True, self.err_mes_not_serialized)

    def _create_quadrant_by_attrs(self, height: Any, width: Any, border_point: Any):
        self.test_create_area_model()

        Quadrant.objects.all().delete()

        quad_attrs = {
            'height': height,
            'width': width,
            'border_point': border_point,
        }

        self.quad = Quadrant.objects.create(**quad_attrs)
        self.quad.full_clean()
        self.assertIsNotNone(self.quad, self.err_mes_not_saved)

        quad_ser = QuadrantSerializer(data=quad_attrs)
        self.assertEqual(quad_ser.is_valid(), True, self.err_mes_not_serialized)
