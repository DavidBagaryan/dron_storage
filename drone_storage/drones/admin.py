from django.contrib import admin

from .models import Drone, Quadrant, AreaModel

admin.site.register([Drone, Quadrant, AreaModel])
