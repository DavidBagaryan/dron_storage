from django.core.management.base import BaseCommand, CommandError

from drones.models import Drone


class Command(BaseCommand):
    help = 'command to emulate drones flying'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        try:
            self._collect_all_existing_drones()
        except Exception as e:
            raise CommandError(repr(e))

    @staticmethod
    def _collect_all_existing_drones():
        drones = Drone.objects.all()

        if drones:
            for _id, drone in enumerate(drones):
                print('drone:', _id)
                drone.emulate_flight(recount=True)
