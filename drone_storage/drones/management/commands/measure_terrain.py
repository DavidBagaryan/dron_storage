from django.core.management.base import BaseCommand, CommandError

from ._utils import Area

default_coordinates = (100, 150)
default_pieces = 10


class Command(BaseCommand):
    help = 'a command for measure terrain\n measure will be bypassed args or by default'

    area: Area

    def add_arguments(self, parser):
        parser.add_argument(
            *('--measures', '-M'),
            help='geo parameters to apply (height, width)',
            nargs=2,
            type=int,
            default=default_coordinates,
        )
        parser.add_argument(
            *('--pieces', '-P'),
            help='divides the area into pieces. this parameter for count of pieces',
            nargs='?',
            type=int,
            default=default_pieces,
        )

    def handle(self, *args, **options):
        self.area = Area(options)
        self.area.init_piece()

        try:
            self.area.was_area_recalculated()
            print('area square is:', self.area.get_sq())
            print('piece square is:', self.area.piece.get_sq())
        except Exception as e:
            raise CommandError(repr(e))
