import decimal

from drones.models import Quadrant, AreaModel, Drone
from drones.utils import AreaHelper


class Piece(AreaHelper):
    def __init__(self, area: 'Area'):
        self.sq = area.get_sq() / area.pieces

    def __str__(self):
        return f'{self.height}; {self.width}'

    def make_sides(self, area_small_side) -> None:
        self.height = area_small_side
        self.width = self.sq / self.height


class Area(AreaHelper):
    area_model: AreaModel
    piece: Piece

    _flag_recount: bool = False

    def __init__(self, options: dict):
        self.height, self.width = options['measures']
        self.pieces = options['pieces']

        self._check_last_record()

    def init_piece(self) -> None:
        self.piece = Piece(self)
        self.piece.make_sides(self.get_smaller_side())

        Drone.objects.all().delete()
        quadrants = Quadrant.objects.all()

        if quadrants.count() != self.pieces or self._flag_recount:
            self._flag_recount = True
            quadrants.delete()
            full_distance = self.area_model.get_bigger_side()
            gap = decimal.Decimal(self.piece.get_smaller_side())
            border = gap

            for i in range(self.pieces):
                print(border)
                Quadrant.objects.create(
                    height=self.piece.height,
                    width=self.piece.width,
                    border_point=border if border <= full_distance else full_distance
                )
                border += gap

    def was_area_recalculated(self) -> None:
        print('area was recalculated!') if self._flag_recount else None

    def _check_last_record(self) -> None:
        last_area_model = AreaModel.get_latest()

        if last_area_model:
            if last_area_model.get_sq() == self.get_sq():
                self.area_model = last_area_model
                return
            else:
                last_area_model.is_active = 0
                last_area_model.save()

        new_area_model = AreaModel.objects.create(height=self.height, width=self.width, is_active=1)

        self._flag_recount = True
        self.area_model = new_area_model
