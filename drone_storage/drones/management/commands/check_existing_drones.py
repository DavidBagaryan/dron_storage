import random

from django.core.management.base import BaseCommand, CommandError

from drones.models import Drone, Quadrant

default_count: int = 5
max_flight_distance: float = 7.01


class Command(BaseCommand):
    help = "checking drones coordinates"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        try:
            existing_drones = Drone.objects.all()

            cur_xy = {}
            for _id, dr in enumerate(existing_drones, start=1):
                print('drone:', _id)
                new_xy = {dr.x, dr.y}
                print(cur_xy, '\n', new_xy)
                print('match!') if cur_xy == new_xy else None
                cur_xy = new_xy

        except Exception as e:
            raise CommandError(e)
