import random

from django.core.management.base import BaseCommand, CommandError

from drones.models import Drone, Quadrant

default_count: int = 5


class Command(BaseCommand):
    help = "let's add some drones!"

    def add_arguments(self, parser):
        parser.add_argument(
            *('--count', '-C'),
            help='count of drones for add',
            type=int,
            default=default_count,
        )
        parser.add_argument(
            *('--delete', '-D'),
            help='0 means do not delete existing, 1 is delete',
            choices=[0, 1],
            type=int,
            default=0,
        )

    def handle(self, *args, **options):
        quadrants = Quadrant.objects.all()
        quad_ids = [quad.id for quad in quadrants]

        delete_drones = options['delete']

        if delete_drones != 0 and delete_drones == 1:
            existing_drones = Drone.objects.all()
            if existing_drones.count() == 0:
                print('no drones detected\n')
            else:
                existing_drones.delete()

        for n in range(1, options['count'] + 1):
            print('drone:', n)
            x, y = Drone.get_initials()
            quad = Quadrant.objects.get(pk=random.choice(quad_ids))
            drone = Drone.objects.create(x=x, y=y, quadrant=quad)
            drone.emulate_flight()
