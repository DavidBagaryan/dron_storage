from rest_framework import generics

from .models import Drone
from .serializers import DroneSerializer


class DroneListView(generics.ListCreateAPIView):
    queryset = Drone.objects.all()
    serializer_class = DroneSerializer


class DroneListViewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Drone.objects.all()
    serializer_class = DroneSerializer
    lookup_url_kwarg = 'id'
    lookup_field = 'id'
