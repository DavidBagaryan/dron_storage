import decimal
import random

from django.db import models, utils
from django.utils.crypto import get_random_string
from rest_framework.exceptions import ValidationError

from .utils import AreaHelper

MAX_DIGITS = 18
PRECISION = 5


class Drone(models.Model):
    class Meta:
        ordering = ['-last_report']
        unique_together = ('x', 'y')

    iterator = models.AutoField(auto_created=True, primary_key=True)

    id = models.CharField(max_length=100, db_index=True, unique=True)
    x = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)
    y = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)

    quadrant = models.ForeignKey('Quadrant', related_name='drones', on_delete=models.DO_NOTHING)
    last_report = models.DateTimeField(auto_now_add=True, db_index=True)
    err_log = models.TextField(blank=True)

    def __str__(self):
        return f'ID: {self.id}: {self.x}, | {self.y}\nQUAD: {self.quadrant} || {self.iterator}'

    def save(self, *args, **kwargs):
        """some custom saving for Drone's ID"""

        if self.pk is None:
            self.id = get_random_string(length=32)
        super().save(*args, **kwargs)

    def emulate_flight(self, recount=False):
        """drone flight emulator"""

        if recount:
            x, y = Drone.get_randoms(7.01)
            self.x += x
            self.y += y

        try:
            self.clean()
            self.save()
        except ValidationError as ve:
            errs = ve.args[0] if len(ve.args) > 0 else None

            if 'recommend_value' in errs:
                self.x = errs['recommend_value']

            if 'nearest_quadrant' and 'x' in errs:
                recommended_quad_id = errs['nearest_quadrant']
                self.quadrant = Quadrant.objects.get(pk=recommended_quad_id)

            if 'possible_value' and 'y' in errs:
                pos_val = errs['possible_value']
                new_y = random.randrange(0, pos_val) if pos_val > 0 else 0
                self.y = new_y

            self.emulate_flight()
        except utils.IntegrityError:
            self.emulate_flight(recount=True)
        else:
            print(f'drone ID: {self.id} ON AIR')

    def clean(self):
        """django hook for custom model validation"""

        self._validate_x()
        self._validate_y()

    @staticmethod
    def get_initials():
        """make initial params for creating drone"""

        area_model = AreaModel.get_latest()
        x = random.uniform(0, float(area_model.get_bigger_side()))
        y = random.uniform(0, float(area_model.get_smaller_side()))

        return decimal.Decimal(x), decimal.Decimal(y)

    @staticmethod
    def get_randoms(flight_distance: float) -> tuple:
        """get random drone coordinates to emulate moving"""

        flight_distance = flight_distance
        x = random.uniform(-flight_distance, flight_distance)
        plus_or_minus = random.choice('-+')

        y = str(flight_distance - x) if x >= 0 else str(flight_distance + x)
        y = float(plus_or_minus + y)

        return decimal.Decimal(x), decimal.Decimal(y)

    def _validate_x(self):
        """custom validation x coordinate parameter by position in related quadrant and whole area"""

        max_side = AreaModel.get_latest().get_bigger_side()
        recommend_value = 0 if self.x < 0 else max_side if self.x > max_side else None

        if recommend_value is not None:
            message = f'get back you! bad, bad, drone... out of area, recommended value is {recommend_value}'
            raise ValidationError({
                'x': message,
                'recommend_value': recommend_value
            })

        previous_quad = Quadrant.objects.filter(pk=self.quadrant.id - 1)
        max_value = self.quadrant.border_point
        min_value = previous_quad[0].border_point if previous_quad.exists() else 0

        if self.x < min_value or self.x > max_value:
            small_side = self.quadrant.get_smaller_side()
            recommended = Quadrant.get_nearest_quad(self)
            err_mes = f'possible values from 0 to {small_side} for selected quadrant'

            raise ValidationError({
                'x': err_mes,
                'nearest_quadrant': recommended.id,
                'max_value': small_side
            })

    def _validate_y(self):
        """custom validation y coordinate parameter by position in related quadrant's height"""

        big_side = self.quadrant.get_bigger_side()
        possible_value = big_side if self.y > big_side else 0 if self.y < 0 else None

        if possible_value is not None:
            raise ValidationError({
                'y': f"you can change position, but you can't fly away. possible values from 0 to {big_side}",
                'possible_value': possible_value,
            })


class Quadrant(models.Model, AreaHelper):
    height = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)
    width = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)
    date_creating = models.DateTimeField(auto_now_add=True, db_index=True)

    border_point = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)

    def __str__(self):
        return f'ID: {self.id} | {self.border_point}, | {self.get_bigger_side()}'

    @staticmethod
    def get_nearest_quad(drone: Drone) -> 'Quadrant':
        """retrieves the nearest quadrant by drone.x position parameter"""

        quads = Quadrant.objects.order_by('border_point')
        quad = quads.filter(border_point__gt=drone.x)
        return quad.first() if quad.exists() else quads.last()


class AreaModel(models.Model, AreaHelper):
    class Meta:
        ordering = ['-date_creating']

    height = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)
    width = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=PRECISION)
    date_creating = models.DateTimeField(auto_now_add=True, db_index=True)

    is_active = models.SmallIntegerField()

    def __str__(self):
        return f'{self.date_creating} | is active: {self.is_active}'

    @classmethod
    def get_latest(cls):
        """retrieves the latest area model"""

        return cls.objects.filter(is_active=1).last()
