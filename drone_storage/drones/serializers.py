from rest_framework import serializers

from .models import Drone, Quadrant


class DroneSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)

    def validate(self, attrs):
        drone = Drone(**attrs)
        drone.clean()
        return attrs

    class Meta:
        model = Drone
        fields = ('x', 'y', 'id', 'quadrant')


class QuadrantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quadrant
        fields = ('id',)
