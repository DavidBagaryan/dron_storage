#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for drone_playground DB started..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "drone_playground DB started"
fi

python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --no-input

echo "from django.contrib.auth.models import User;" \
     "User.objects.filter(email='admin@example.com').delete();" \
     " User.objects.create_superuser('admin', 'admin@example.com', '321456')" | python manage.py shell

# run tests
python manage.py test

# common initials
python manage.py measure_terrain -M 1753 739 -P 19
python manage.py make_some_drones -D 1 -C 23

# add cron-job
python manage.py crontab add

# run system cron
crond -b -d 8

exec "$@"
